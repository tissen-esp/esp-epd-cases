# ESP-EPD-Cases

This repository serves as a collection of DIY E-Paper cases integrated with ESP microcontrollers, tailored for diverse applications.

## Current linked Projects

This is a incomplete list of projects that use this collection:
- [ESP-WeatherStation](https://gitlab.com/tissen-esp/esp-weatherstation)
- [ESP-Google-KalenderViewer](https://gitlab.com/tissen-esp/esp-google-kalenderview)
- [ESP-TodoViewer-MSToDo](https://gitlab.com/tissen-esp/esp-todoviewer-mstodo)
- ...

## Cases

| Name | Description | Showcase |
| ---- | ----------- | -------- |
| [Case-01](epd-7.5/case-01) | A Picture Frame with a 13x18cm Frame from Rossman and 3D printed inlet| <img src="epd-7.5/case-01/front.png" alt="drawing" width="400"/> |
| [Case-02](epd-7.5/case-02) | A Picture Frame with a 13x18cm Frame from Rossman and 3D printed inlet with retrofitted utils for easier 3d printing | <img src="epd-7.5/case-02/case.png" alt="drawing" width="400"/> |

## Contributions are welcome!
We invite you to improve this project with us. Whether you are a programming genius or a newbie in this field, your ideas, bug fixes and improvements are important to us. Let's work together and make this project even better together!

## License
If Not otherwise stated in the specific folder, all files in this repository are licensed under the GNU GENERAL PUBLIC LICENE. See [LICENSE](LICENSE) for more information.