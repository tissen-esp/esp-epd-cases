# PictureFrame-01

This Case is designed for the Waveshare 7.5inch E-Paper Display with __uPesy__ holder and a Picture Frame from Rossman.

## Additional Hardware
- Thread insert 4x M2 and 4x M2.5
- Screws 4x M2 and 4x M2.5
- Picture Frame MDF 13x18cm from Rossman [Rossman link](https://www.rossmann.de/de/haushalt-fotowelt-bilderrahmen-mdf-13x18-cm-weiss/p/4305615709659) or similar.
- White Filament for a 3D printer to create the inlet.

## STL-Files for 3D-Printing

![alt text](front.png)
![alt text](back.png)

## Contributer
- [Stephan Brüning](https://gitlab.com/Stebruening) 
- [Tissen](https://gitlab.com/Tissen)