# PictureFrame-01

This Case is designed for the Waveshare 7.5inch E-Paper Display and a Picture Frame from Rossman. 
As a ESP32 the __uPesy__ is supported but with minor changes in the utils file other ESP32 can be used.

## Additional Hardware
- Thread insert 4x M2 and 4x M2.5
- Screws 4x M2 and 4x M2.5
- Picture Frame MDF 13x18cm from Rossman [Rossman link](https://www.rossmann.de/de/haushalt-fotowelt-bilderrahmen-mdf-13x18-cm-weiss/p/4305615709659) or similar.
- White Filament for a 3D printer to create the inlet.

## STL-Files for 3D-Printing

### Main Case
![alt text](case.png)

### Frame
![alt text](frame.png)

### Battery Holder & EPD-Driver Holder & Upesy Holder 
![alt text](utils.png)

## Contributer
- [Brüning](https://gitlab.com/Stebruening) 
- [Tissen](https://gitlab.com/Tissen)